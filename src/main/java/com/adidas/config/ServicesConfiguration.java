package com.adidas.config;

public class ServicesConfiguration{

    // Base URI
    public static final String URI = "https://petstore.swagger.io/v2";

    // Endpoints
    public static final String PET = "/pet";
    public static final String STORE = "/store";
    public static final String USER = "/user";

    private ServicesConfiguration () {
        throw new IllegalStateException("Utility class");
    }
}